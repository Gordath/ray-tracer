#ifndef PLANE_H_
#define PLANE_H_

#include "vec.h"
#include "object.h"

class Plane : public Object {
public:
	Vector3 normal;
	float dist;

	Plane();
	Plane(const Vector3 &norm, float dist);

	bool intersect(const Ray &ray, HitPoint *pt) const;
};

#endif	// PLANE_H_
