#ifndef MATERIAL_H_
#define MATERIAL_H_

#include "color.h"

class Material {
public:
	// phong model parameters
	Color diffuse;    // amount of diffuse light scattering (per color channel)
	Color specular;   // amount of specular light reflection (per color channel)
	float shininess;  // higher shininess values -> more focused specular refl.

	// additional raytracing parameters
	float reflectivity; // range [0, 1]
	float transparency; // range [0, 1]
	float ior;          // index of refraction

	Material();
	Material(const Color &dcol, const Color &scol, float spow);
};

#endif	// MATERIAL_H_
