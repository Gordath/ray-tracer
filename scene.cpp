#include <float.h>
#include "scene.h"

Scene::Scene()
{
	cam = 0;
}

Scene::~Scene()
{
	for(int i=0; i<(int)objects.size(); i++) {
		delete objects[i];
	}
}

void Scene::add_object(Object *obj)
{
	objects.push_back(obj);
}

void Scene::add_light(Light *lt)
{
	lights.push_back(lt);
}

Light *Scene::get_light(int idx) const
{
	if(idx >= 0 && idx < (int)lights.size()) {
		return lights[idx];
	}
	return 0;
}

int Scene::get_num_lights() const
{
	return (int)lights.size();
}

void Scene::set_camera(Camera *cam)
{
	this->cam = cam;
}

Camera *Scene::get_camera() const
{
	return cam;
}

bool Scene::find_intersection(const Ray &ray, HitPoint *hit)
{
	HitPoint first_hit;
	first_hit.dist = FLT_MAX;
	first_hit.obj=0;

	for(int i=0;i<(int)objects.size();i++)
	{
		HitPoint hit;
		if(objects[i]->intersect(ray,&hit) && hit.dist<first_hit.dist)
		{
			first_hit=hit;
		}
	}

	if(first_hit.obj!=0)
	{
		*hit=first_hit;
		return true;
	}
	return false;
}
