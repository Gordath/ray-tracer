#ifndef RAY_H_
#define RAY_H_

#include "vec.h"
#include "matrix.h"

class Ray {
public:
	Vector3 origin, dir;

	Ray();
	Ray(const Vector3 &origin, const Vector3 &dir);
};

Ray transform(const Ray &ray, const Matrix4x4 &mat);

#endif	// RAY_H_
