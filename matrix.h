#ifndef MATRIX_H_
#define MATRIX_H_

class Matrix4x4 {
public:
	// [column] [row]
	float m[4][4];

	Matrix4x4();
	Matrix4x4(float a1, float a2, float a3, float a4,
			float b1, float b2, float b3, float b4,
			float c1, float c2, float c3, float c4,
			float d1, float d2, float d3, float d4);

	void set_identity();
	void translate(float x, float y, float z);
	void rotate_x(float angle);
	void rotate_y(float angle);
	void rotate_z(float angle);
	void rotate(float angle, float x, float y, float z);
	void scale(float x, float y, float z);

	void perspective(float vfov, float aspect, float near_clip, float far_clip);

	void transpose();
};

Matrix4x4 operator *(const Matrix4x4 &a, const Matrix4x4 &b);

#endif	// MATRIX_H_
