#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifdef WIN32
#include "glut.h"
#elif defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "raytracer.h"

static bool init();
static void cleanup();
static void disp();
static void update_texture(const float *fb);
static void reshape(int x, int y);
static void keyb(unsigned char key, int x, int y);
static int next_pow2(int x);

static unsigned int tex;
static int xsz = 800;
static int ysz = 450;
static int tex_xsz, tex_ysz;
static bool must_render = true;

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(xsz, ysz);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutCreateWindow("raytracer");

	glutDisplayFunc(disp);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyb);

	if(!init()) {
		return 1;
	}
	atexit(cleanup);

	glutMainLoop();
	return 0;
}

static bool init()
{
	if(!ray_init(xsz, ysz)) {
		return false;
	}

	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glEnable(GL_TEXTURE_2D);
	return true;
}

static void cleanup()
{
	ray_cleanup();
}

static void disp()
{
	if(must_render) {
		unsigned int start = glutGet(GLUT_ELAPSED_TIME);
		float *framebuf = ray_render();
		unsigned int dur = glutGet(GLUT_ELAPSED_TIME) - start;

		printf("rendering took: %u ms\n", dur);

		update_texture(framebuf);
	}

	glBegin(GL_QUADS);
	glTexCoord2f(0, 1);
	glVertex2f(-1, -1);
	glTexCoord2f(1, 1);
	glVertex2f(1, -1);
	glTexCoord2f(1, 0);
	glVertex2f(1, 1);
	glTexCoord2f(0, 0);
	glVertex2f(-1, 1);
	glEnd();

	glutSwapBuffers();
}

static void update_texture(const float *fb)
{
	int ntx = next_pow2(xsz);
	int nty = next_pow2(ysz);

	if(ntx != tex_xsz || nty != tex_ysz) {
		tex_xsz = ntx;
		tex_ysz = nty;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_xsz, tex_ysz, 0, GL_RGB, GL_FLOAT, 0);
	}

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, xsz, ysz, GL_RGB, GL_FLOAT, fb);
}

static void reshape(int x, int y)
{
	int tx, ty;

	xsz = x;
	ysz = y;

	glViewport(0, 0, x, y);

	/* setup the texture matrix that maps just the visible area
	 * of the texture to [0, 1]
	 */
	tx = next_pow2(xsz);
	ty = next_pow2(ysz);

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glScalef((float)xsz / tx, (float)ysz / ty, 1.0f);

	ray_resize(xsz, ysz);
	must_render = true;
}

static void keyb(unsigned char key, int x, int y)
{
	switch(key) {
	case 27:
		exit(0);

	case 'r':
		must_render = true;
		glutPostRedisplay();
		break;
	}
}

static int next_pow2(int x)
{
	x--;
	x = (x >> 1) | x;
	x = (x >> 2) | x;
	x = (x >> 4) | x;
	x = (x >> 8) | x;
	x = (x >> 16) | x;
	return x + 1;
}
