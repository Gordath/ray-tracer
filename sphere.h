#ifndef SPHERE_H_
#define SPHERE_H_

#include "vec.h"
#include "object.h"

class Sphere : public Object {
public:
	Vector3 pos;
	float radius;

	Sphere();
	Sphere(const Vector3 &pos, float rad);

	bool intersect(const Ray &ray, HitPoint *pt) const;
};

#endif	// SPHERE_H_
