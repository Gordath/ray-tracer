#include "ray.h"

Ray::Ray()
{
}

Ray::Ray(const Vector3 &origin_, const Vector3 &dir_)
	: origin(origin_), dir(dir_)
{
}

Ray transform(const Ray &ray, const Matrix4x4 &mat)
{
	Matrix4x4 dirmat = mat;
	dirmat.m[3][0] = dirmat.m[3][1] = dirmat.m[3][2] = 0.0;

	return Ray(transform(ray.origin, mat), transform(ray.dir, dirmat));
}
