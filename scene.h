#ifndef SCENE_H_
#define SCENE_H_

#include <vector>
#include "object.h"
#include "light.h"
#include "camera.h"

class Scene {
private:
	std::vector<Object*> objects;
	std::vector<Light*> lights;
	Camera *cam;

public:
	Scene();
	~Scene();

	void add_object(Object *obj);

	void add_light(Light *lt);
	Light *get_light(int idx) const;
	int get_num_lights() const;

	void set_camera(Camera *cam);
	Camera *get_camera() const;

	bool find_intersection(const Ray &ray, HitPoint *hit);
};

#endif	// SCENE_H_
