#define _USE_MATH_DEFINES
#include <math.h>
#include "camera.h"

Camera::Camera()
{
	set_fov(45);
}

Camera::Camera(const Vector3 &pos, const Vector3 &targ)
{
	this->pos = pos;
	target = targ;
	set_fov(45);
}

void Camera::set_fov(float fov_deg)
{
	fov = M_PI * fov_deg / 180.0;
}

Ray Camera::get_primary_ray(int x, int y, int width, int height) const
{
	float aspect=(float)width/(float) height;

	Ray ray;
	ray.origin=Vector3(0,0,0);
	ray.dir.x=(2.0*(float)x/(float)width-1.0)*aspect;
	ray.dir.y=1.0-2.0*(float)y/(float)height;
	ray.dir.z=1.0/tan(fov/2.0);


	Vector3 dir=target-pos;
	dir.normalize();
	Vector3 up=Vector3(0,1,0);
	Vector3 right=cross(up,dir);
	right.normalize();
	up=cross(dir,right);

	Matrix4x4 mat(right.x,up.x,dir.x,pos.x,
					right.y,up.y,dir.y,pos.y,
					right.z,up.z,dir.z,pos.z,
					0,0,0,1);

	return transform(ray,mat);
}
