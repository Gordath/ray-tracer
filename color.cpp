#include "color.h"

Color::Color()
{
	r = g = b = 0.0f;
}

Color::Color(float r, float g, float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

Color operator +(const Color &c1, const Color &c2)
{
	return Color(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b);
}

Color operator *(const Color &c1, const Color &c2)
{
	return Color(c1.r * c2.r, c1.g * c2.g, c1.b * c2.b);
}

Color operator *(const Color &col, float s)
{
	return Color(col.r * s, col.g * s, col.b * s);
}

Color operator /(const Color &col, float s)
{
	return Color(col.r / s, col.g / s, col.b / s);
}
