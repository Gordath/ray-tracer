#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "raytracer.h"
#include "scene.h"
#include "sphere.h"
#include "plane.h"

#define MAX_ITER	5

static Color ray_trace(const Ray &ray, int iter);
static Color shade(const Ray &ray, const HitPoint *hit, int iter);

static int fb_width, fb_height;
static Color *fb_pixels;
static Scene *scn;

bool ray_init(int width, int height)
{
	ray_resize(width, height);

	// create scene
	scn = new Scene;
		Sphere *redsph = new Sphere(Vector3(0, 1, 0), 1);
	redsph->material.diffuse = Color(1, 0, 0);
	redsph->material.specular = Color(0.8, 0.8, 0.8);
	redsph->material.shininess = 40.0;
	scn->add_object(redsph);

	Sphere *sph2 = new Sphere(Vector3(-3, 2.5, 2), 1.5);
	sph2->material.diffuse = Color(0.1, 0.15, 0.3);
	sph2->material.specular = Color(0.2, 0.4, 0.8);
	sph2->material.shininess = 40.0;
	sph2->material.reflectivity = 0.8;
	scn->add_object(sph2);

	Sphere *sph3 = new Sphere(Vector3(-2, 1.5, -3), 0.7);
	sph3->material.diffuse = Color(0.8, 0.75, 0.2);
	sph3->material.specular = Color(0.7, 0.7, 0.7);
	scn->add_object(sph3);

	Plane *floor = new Plane(Vector3(0, 1, 0), 0);
	floor->material.diffuse = Color(0.1, 0.4, 0.1);
	floor->material.reflectivity = 0.5;
	scn->add_object(floor);

	Plane *mirror = new Plane(Vector3(-1, 0, -1), -6.5);
	mirror->material.diffuse = Color(0.1, 0.1, 0.1);
	mirror->material.specular = Color(0.8, 0.8, 0.8);
	mirror->material.shininess = 30.0;
	mirror->material.reflectivity = 0.8;
	scn->add_object(mirror);

	Light *lt = new Light(Vector3(-6, 10, -7), Color(0.8, 0.8, 0.8));
	scn->add_light(lt);

	Light *backlt = new Light(Vector3(5, 6, 3), Color(0.2, 0.3, 0.4));
	scn->add_light(backlt);

	Camera *cam = new Camera(Vector3(0, 3, -7), Vector3(0, 1, 0));
	scn->set_camera(cam);


	/*
	Sphere *redsph = new Sphere(Vector3(0, 1, 0), 1);
	redsph->material.diffuse = Color(1, 0, 0);
	redsph->material.specular = Color(0.8, 0.8, 0.8);
	redsph->material.shininess = 40.0;
	scn->add_object(redsph);

	Sphere *sph3 = new Sphere(Vector3(-2, 1.5, -3), 0.7);
	sph3->material.diffuse = Color(0.8, 0.75, 0.2);
	sph3->material.specular = Color(0.7, 0.7, 0.7);
	scn->add_object(sph3);

	Plane *floor = new Plane(Vector3(0, 1, 0), 0);
	floor->material.diffuse = Color(0.1, 0.4, 0.1);
	floor->material.reflectivity = 0.5;
	scn->add_object(floor);

	Light *lt = new Light(Vector3(-6, 10, -7), Color(0.8, 0.8, 0.8));
	scn->add_light(lt);

	Light *backlt = new Light(Vector3(5, 6, 3), Color(0.2, 0.3, 0.4));
	scn->add_light(backlt);

	Camera *cam = new Camera(Vector3(0, 3, -7), Vector3(0, 1, 0));
	scn->set_camera(cam);
*/
	return true;
}

void ray_cleanup()
{
	delete [] fb_pixels;
	fb_pixels = 0;

	delete scn;
}

void ray_resize(int width, int height)
{
	delete [] fb_pixels;
	fb_pixels = new Color[width * height];

	fb_width = width;
	fb_height = height;
}

float *ray_render()
{
	Camera *cam = scn->get_camera();
	if(!cam) {
		fprintf(stderr, "no camera found!\n");
		abort();
	}

	Color *pixptr = fb_pixels;
	for(int i=0; i<fb_height; i++) {
		for(int j=0; j<fb_width; j++) {
			Ray ray = cam->get_primary_ray(j, i, fb_width, fb_height);
			*pixptr++ = ray_trace(ray, 0);
		}
	}

	return (float*)fb_pixels;
}

static Color ray_trace(const Ray &ray, int iter)
{
	HitPoint hit;
	if(iter>MAX_ITER || !scn->find_intersection(ray,&hit))
	{
		return Color(0,0,0);
	}

	return shade(ray,&hit,iter);
}

static Color shade(const Ray &ray, const HitPoint *hit, int iter)
{
	const Material *mat=&hit->obj->material;
	
	int num_lights=scn->get_num_lights();
	
	Vector3 Vdir=-ray.dir;
	Vdir.normalize();
	Vector3 rdir=reflect(Vdir,hit->normal);

	Color color;

	for(int i=0;i<num_lights;i++)
	{
		Light *lt=scn->get_light(i);
		Vector3 Ldir=lt->pos-hit->pos;
		Ray shadow_ray;
		shadow_ray.origin=hit->pos;
		shadow_ray.dir=Ldir;

		HitPoint shadow_hit;
		if(scn->find_intersection(shadow_ray,&shadow_hit) && shadow_hit.dist<1.0)
		{
			continue;
		}

		Ldir.normalize();

		float ndotl=std::max(dot(Ldir,hit->normal),0.0f);
		Color diffuse=mat->diffuse*ndotl;

		float rdotl=std::max(dot(rdir,Ldir),0.0f);

		Color specular=mat->specular*pow(rdotl,mat->shininess);

		color=color+(diffuse+specular)*lt->color;
	}
	if(mat->reflectivity>0.0001)
		{
			Ray refl_ray;
			refl_ray.origin=hit->pos;
			refl_ray.dir=reflect(Vdir,hit->normal);
			color=color+ray_trace(refl_ray,iter+1)*mat->reflectivity;
		}

	return color;
}
