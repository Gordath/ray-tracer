#ifndef CAMERA_H_
#define CAMERA_H_

//#include "vec.h"
#include "ray.h"

class Camera {
private:
	Vector3 pos, target;
	float fov;

public:
	Camera();
	Camera(const Vector3 &pos, const Vector3 &targ);

	void set_fov(float fov_deg);

	Ray get_primary_ray(int x, int y, int width, int height) const;
};

#endif	// CAMERA_H_
