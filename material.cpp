#include "material.h"

Material::Material()
	: diffuse(1.0, 1.0, 1.0), specular(0.0, 0.0, 0.0)
{
	shininess = 60.0;
	reflectivity = 0.0;
	transparency = 0.0;
	ior = 1.0;
}

Material::Material(const Color &dcol, const Color &scol, float spow)
	: diffuse(dcol), specular(scol)
{
	shininess = spow;
	reflectivity = 0.0f;
	transparency = 0.0f;
	this->ior = 1.0f;
}
