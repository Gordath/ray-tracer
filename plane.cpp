#include <math.h>
#include "plane.h"

Plane::Plane()
	: normal(0.0, 1.0, 0.0)
{
	dist = 0.0;
}

Plane::Plane(const Vector3 &norm, float dist)
	: normal(norm)
{
	normal.normalize();
	this->dist = dist;
}

bool Plane::intersect(const Ray &ray, HitPoint *pt) const
{
	float ndotdir=dot(normal,ray.dir);
	
	if(fabs(ndotdir)<0.0001)
	{
		return false;
	}
	Vector3 planept=normal*dist;
	Vector3 pptdir=planept-ray.origin;

	float t=dot(normal,pptdir)/ndotdir;

	if(t<0.00001)
	{
		return false;
	}

	pt->obj=this;
	pt->dist=t;
	pt->pos=ray.origin+ray.dir*t;
	pt->normal=normal;

	return true;
}
