csrc = $(wildcard *.c)
cppsrc = $(wildcard *.cpp)

obj = $(csrc:.c=.o) $(ccsrc:.cc=.o) $(cppsrc:.cpp=.o)
dep = $(obj:.o=.d)

bin = raytracer

#opt = -O3 -ffast-math
dbg = -g
incpaths = -Isrc -Ilibs -Ilibs/vorbis -Ilibs/vmath -Ilibs/imago
libpaths = -L/usr/local/lib

CFLAGS = -pedantic -Wall $(dbg) $(opt) $(incpaths)
CXXFLAGS = $(CFLAGS)
LDFLAGS = $(libgl) $(libal) $(extlibs) -lm -lpthread -ldl -lz -lpng -ljpeg

ifeq ($(shell uname -s), Darwin)
	libgl = -framework OpenGL -framework GLUT -lGLEW
	libal = -framework OpenAL
else
	libgl = -lGL -lGLU -lglut -lGLEW
	libal = -lopenal
endif

$(bin): $(obj) $(extlibs)
	$(CXX) -o $@ $(obj) $(LDFLAGS)

$(extlibs):
	$(MAKE) -C $(@:%.a=%)

-include $(dep)

%.d: %.c
	@$(CPP) $(CFLAGS) $< -MM -MT $(@:.d=.o) >$@

%.d: %.cc
	@$(CPP) $(CXXFLAGS) $< -MM -MT $(@:.d=.o) >$@

%.d: %.cpp
	@$(CPP) $(CXXFLAGS) $< -MM -MT $(@:.d=.o) >$@

.PHONY: clean
clean:
	rm -f $(obj) $(bin) $(dep)
