#include "light.h"

Light::Light()
{
	color = Color(1, 1, 1);
}

Light::Light(const Vector3 &pos, const Color &col)
{
	this->pos = pos;
	color = col;
}
