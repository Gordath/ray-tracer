#include <math.h>
#include "vec.h"
#include "matrix.h"

Vector3::Vector3()
{
	x = y = z = 0.0f;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3 Vector3::operator -() const
{
	return Vector3(-x, -y, -z);
}

float Vector3::length() const
{
	return sqrt(x * x + y * y + z * z);
}

void Vector3::normalize()
{
	float len = length();
	if(fabs(len) > 1e-5) {
		x /= len;
		y /= len;
		z /= len;
	}
}

Vector3 operator +(const Vector3 &a, const Vector3 &b)
{
	return Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector3 operator -(const Vector3 &a, const Vector3 &b)
{
	return Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
}

Vector3 operator *(const Vector3 &a, float s)
{
	return Vector3(a.x * s, a.y * s, a.z * s);
}

Vector3 operator /(const Vector3 &a, float s)
{
	return Vector3(a.x / s, a.y / s, a.z / s);
}

float dot(const Vector3 &a, const Vector3 &b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector3 cross(const Vector3 &a, const Vector3 &b)
{
	return Vector3(a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x);
}

Vector3 transform(const Vector3 &v, const Matrix4x4 &m)
{
	float x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0];
	float y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1];
	float z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2];
	return Vector3(x, y, z);
}

Vector3 reflect(const Vector3 &v, const Vector3 &n)
{
	return n * 2.0 * dot(v, n) - v;
}
