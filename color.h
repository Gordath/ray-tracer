#ifndef COLOR_H_
#define COLOR_H_

class Color {
public:
	float r, g, b;

	Color();
	Color(float r, float g, float b);
};

Color operator +(const Color &c1, const Color &c2);
Color operator *(const Color &c1, const Color &c2);
Color operator *(const Color &col, float s);
Color operator /(const Color &col, float s);

#endif	// COLOR_H_
