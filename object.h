#ifndef OBJECT_H_
#define OBJECT_H_

#include "vec.h"
#include "ray.h"
#include "material.h"

struct HitPoint;

class Object {
public:
	Material material;		// surface material properties

	virtual ~Object() {}

	virtual bool intersect(const Ray &ray, HitPoint *pt) const = 0;
};

struct HitPoint {
	float dist;        // parametric distance of intersection along the ray
	Vector3 pos;       // world position of the intersection point
	Vector3 normal;    // surface normal vector at the intersection point
	const Object *obj; // pointer to the intersected object
};

#endif	// OBJECT_H_
