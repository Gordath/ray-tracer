#ifndef RAYTRACER_H_
#define RAYTRACER_H_

bool ray_init(int width, int height);
void ray_cleanup();

void ray_resize(int width, int height);

float *ray_render();

#endif	// RAYTRACER_H_
