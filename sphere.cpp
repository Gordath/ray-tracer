#include <math.h>
#include <stdio.h>
#include "sphere.h"
#include <algorithm>

Sphere::Sphere()
{
	radius = 1.0;
}

Sphere::Sphere(const Vector3 &pos, float rad)
{
	radius = rad;
	this->pos = pos;
}

bool Sphere::intersect(const Ray &ray, HitPoint *pt) const
{
	float a=dot(ray.dir,ray.dir);
	float b=2.0*ray.dir.x*(ray.origin.x-pos.x)+2.0*ray.dir.y*(ray.origin.y-pos.y)+
				2.0*ray.dir.z*(ray.origin.z-pos.z);
	float c=dot(ray.origin,ray.origin)+dot(pos,pos)-2.0*dot(ray.origin,pos)-radius*radius;

	float discr=b*b-4.0*a*c;

	if(discr < 0.00001)
	{
		return false;
	}

	float sqrt_discr=sqrt(discr);
	float t0=(-b+sqrt_discr)/(2.0*a);
	float t1=(-b-sqrt_discr)/(2.0*a);

	float t=std::min(t0,t1);

	if(t<0.00001)
	{
		return false;
	}

	pt->obj=this;
	pt->dist=t;
	pt->pos=ray.origin+ray.dir*t;
	pt->normal=(pt->pos-pos)/radius;
	return true;

}
