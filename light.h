#ifndef LIGHT_H_
#define LIGHT_H_

#include "vec.h"
#include "color.h"

class Light {
public:
	Vector3 pos;
	Color color;

	Light();
	Light(const Vector3 &pos, const Color &col);
};

#endif	// LIGHT_H_
