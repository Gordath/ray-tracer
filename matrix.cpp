#define _USE_MATH_DEFINES
#include <math.h>
#include "matrix.h"

static float deg_to_rad(float x)
{
	return M_PI * x / 180.0;
}

Matrix4x4::Matrix4x4()
{
	set_identity();
}

Matrix4x4::Matrix4x4(float a1, float a2, float a3, float a4,
		float b1, float b2, float b3, float b4,
		float c1, float c2, float c3, float c4,
		float d1, float d2, float d3, float d4)
{
	m[0][0] = a1; m[1][0] = a2; m[2][0] = a3; m[3][0] = a4;
	m[0][1] = b1; m[1][1] = b2; m[2][1] = b3; m[3][1] = b4;
	m[0][2] = c1; m[1][2] = c2; m[2][2] = c3; m[3][2] = c4;
	m[0][3] = d1; m[1][3] = d2; m[2][3] = d3; m[3][3] = d4;
}

void Matrix4x4::set_identity()
{
	*this = Matrix4x4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
}

void Matrix4x4::translate(float x, float y, float z)
{
	Matrix4x4 tmat(1, 0, 0, x,
			0, 1, 0, y,
			0, 0, 1, z,
			0, 0, 0, 1);
	*this = *this * tmat;
}

void Matrix4x4::rotate_x(float angle)
{
	float sa = sin(deg_to_rad(angle));
	float ca = cos(deg_to_rad(angle));
	Matrix4x4 rmat(1, 0, 0, 0,
			0, ca, -sa, 0,
			0, sa, ca, 0,
			0, 0, 0, 1);
	*this = *this * rmat;
}

void Matrix4x4::rotate_y(float angle)
{
	float sa = sin(deg_to_rad(angle));
	float ca = cos(deg_to_rad(angle));
	Matrix4x4 rmat(ca, 0, sa, 0,
			0, 1, 0, 0,
			-sa, 0, ca, 0,
			0, 0, 0, 1);
	*this = *this * rmat;
}

void Matrix4x4::rotate_z(float angle)
{
	float sa = sin(deg_to_rad(angle));
	float ca = cos(deg_to_rad(angle));
	Matrix4x4 rmat(ca, -sa, 0, 0,
			sa, ca, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
	*this = *this * rmat;
}

void Matrix4x4::rotate(float angle, float x, float y, float z)
{
	float sa = sin(deg_to_rad(angle));
	float ca = cos(deg_to_rad(angle));
	float invca = 1.0 - ca;
	float xsq = x * x;
	float ysq = y * y;
	float zsq = z * z;

	Matrix4x4 rmat;
	rmat.m[0][0] = xsq + (1.0 - xsq) * ca;
	rmat.m[1][0] = x * y * invca - z * sa;
	rmat.m[2][0] = x * z * invca + y * sa;

	rmat.m[0][1] = x * y * invca + z * sa;
	rmat.m[1][1] = ysq + (1.0 - ysq) * ca;
	rmat.m[2][1] = y * z * invca - x * sa;

	rmat.m[0][2] = x * z * invca - y * sa;
	rmat.m[1][2] = y * z * invca + x * sa;
	rmat.m[2][2] = zsq + (1.0 - zsq) * ca;

	*this = *this * rmat;
}

void Matrix4x4::scale(float x, float y, float z)
{
	Matrix4x4 smat(x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1);
	*this = *this * smat;
}

void Matrix4x4::perspective(float vfov, float aspect, float znear, float zfar)
{
    vfov = deg_to_rad(vfov);
    float f = 1.0f / tan(vfov * 0.5f);
    float dz = znear - zfar;

	set_identity();

	m[0][0] = f / aspect;
    m[1][1] = f;
    m[2][2] = (zfar + znear) / dz;
    m[3][2] = -1.0f;
    m[2][3] = 2.0f * zfar * znear / dz;
    m[3][3] = 0.0f;
}

void Matrix4x4::transpose()
{
	for(int i=0; i<4; i++) {
		for(int j=0; j<i; j++) {
			float tmp = m[i][j];
			m[i][j] = m[j][i];
			m[j][i] = tmp;
		}
	}
}

Matrix4x4 operator *(const Matrix4x4 &a, const Matrix4x4 &b)
{
	Matrix4x4 res;

	for(int i=0; i<4; i++) {
		for(int j=0; j<4; j++) {
			res.m[i][j] = a.m[0][j] * b.m[i][0] + a.m[1][j] * b.m[i][1] +
				a.m[2][j] * b.m[i][2] + a.m[3][j] * b.m[i][3];
		}
	}
	return res;
}
